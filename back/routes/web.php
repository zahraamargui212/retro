<?php

use App\Http\Controllers\UtilisateurController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::post('/inscription',[UtilisateurController::class,'inscription']);

Route::get('/inscription', function () {
    return view('inscription');
});

Route::get('/connexion', function () {
    return view('connexion');
});

Route::get('/participer', function () {
    return view('participer');
});
Route::get('/afficherFeedBack', function () {
    return view('afficherFeedBack');
});
Route::get('/seDeconnecter', function () {
    return view('seDeconnecter');
});
Route::get('/historique', function () {
    return view('historique');
});
Route::get('/personnesNonparticipe', function () {
    return view('personnesNonparticipe');
});
Route::redirect('/participer','/inscription',301);
Route::redirect('/historique','/inscription',301);

Route::fallback(function(){
echo "Page de la route " .Route::currentRouteName()."n'existe pas";
});