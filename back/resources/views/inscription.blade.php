<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <form action="{{url('inscription')}}" method="post">
        @csrf
        <label for="prenom"> Prénom :</label>
        <input type="text" name="" id="" required>

        <label for="email"> E-mail :</label>
        <input type="email" name="email" id="" required>

        <label for="mdp"> Mot de passe :</label>
        <input type="text" name="mdp" id="" required>

        <input type="submit" value="valider">


    </form>
</body>
</html>