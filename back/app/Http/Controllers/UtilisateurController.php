<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use app\Models\Utilisateur;

class UtilisateurController extends Controller
{
    //
    // public function getDataFrom(){
    //     $m = DB::select('SELECT * FROM utilisateur');
    //     return $m;
    // }

    public function inscription(Request $request){
        $request->validate(['prenom'=>'required']);
        $data=[];
        $data['prenom']=$request->input('prenom');
        $data['email']=$request->input('email');
        $data['mdp']=$request->input('mdp');
        return view('readForm',$data);

    }
}
