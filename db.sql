create database IF NOT EXISTS retro;

CREATE USER IF NOT EXISTS 'retro'@'localhost' IDENTIFIED BY 'pass_retro';

GRANT ALL PRIVILEGES ON retro.* TO 'retro'@'localhost';

FLUSH PRIVILEGES;

use retro;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int PRIMARY KEY,
  `prenom` varchar(255),
  `email` varchar(255),
  `mot_de_passe` varchar(255)
);

CREATE TABLE IF NOT EXISTS `retrospective` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_auteur` INT NOT NULL,
  `titre` varchar(255),
  `date_creation` date,
  `date_debut` date,
  `date_fin` date,
  FOREIGN KEY (id_auteur) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS  `retrospective_users` (
  `id_user` INT NOT NULL,
  `id_retro` INT NOT NULL,
  `date_participation` date,
  PRIMARY KEY (`id_user`, `id_retro`),
  FOREIGN KEY (id_user) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (id_retro) REFERENCES retrospective(id) ON DELETE CASCADE ON UPDATE CASCADE
  
);

CREATE TABLE IF NOT EXISTS  `feedbacks` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_retro` INT NOT NULL,
  `contenus` text,
  `date_creation` date,
  `aime` BOOLEAN,
  FOREIGN KEY (id_retro) REFERENCES retrospective(id) ON DELETE CASCADE ON UPDATE CASCADE
);

